﻿using System;
using System.IO;

namespace Test
{
    internal interface IHaveStream : IDisposable
    {
        Stream GetStream();
    }

    internal interface IHaveStream<TStream> : IHaveStream
    {
        new TStream GetStream();
    }

}
