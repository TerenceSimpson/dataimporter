﻿namespace Test
{
    public sealed class XoserveModel
    {
        public string area;
        public string file_type;
        public string date;
        public string temp1;
        public string mprn;
        public string msn;
        public string principle_street;
        public string sub_building;
        public string dep_street;
        public string building_number;
        public string address_1;
        public string address_2;
        public string address_3;
        public string address_4;
        public string city;
        public string County;
        public string outcode;
        public string incode;
        public string temp2;
        public string post_code;
    }

    public static class XoserveRecordExtension
    {
        public static bool IsNull(this XoserveModel self) => self == null || self.incode == null;
    }
}
