﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Test
{

    namespace Importers
    {
        public sealed class Ecoes : TextImporter<EcoesModel>
        {
            private int lines;

            public int Lines => lines;

            private Ecoes(string path, IHaveStream stream) : base(Path.GetFileName(path), stream)
            {
            }

            public Ecoes(string path, Stream stream) : base(Path.GetFileName(path), stream)
            {
            }

            public Ecoes(string path) : this(path, new MemMapStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 4096 * 4))
            {
            }

            private EcoesModel GetModelFromString(string line)
            {
                lines++;
                if (!String.IsNullOrWhiteSpace(line))
                {
                    var parts = line.Split(new[] { '|' }, StringSplitOptions.None);
                    if (parts.Length >= 5)
                    {
                        var model = NewModel(parts);
                        if (model.meter_type != null)
                        {
                            if (model.meter_type.Equals("K", StringComparison.OrdinalIgnoreCase) || model.meter_type.Equals("S1", StringComparison.OrdinalIgnoreCase))
                            {
                                return model;
                            }
                        }
                    }
                }
                return SkipObject;
            }

            protected override EcoesModel GetModelFromTextReader(TextReader textReader)
            {
                string line = null;
                try
                {
                    line = textReader.ReadLine();

                } catch (ArgumentOutOfRangeException ex)
                {
                    throw;
                }
                if (line != null)
                {
                    return GetModelFromString(line);
                }
                return null;
            }
        }

        public sealed class BufferedEcoes : BufferedImporter<Ecoes, EcoesModel>
        {
            public BufferedEcoes(string path, Stream stream, int bufferSize) : base(new Ecoes(path, stream), bufferSize)
            {
            }

            public BufferedEcoes(string path, int bufferSize) : base(new Ecoes(path), bufferSize)
            {
            }
        }
    }

    class Program
    {
        public sealed class Statistics
        {
            public TextImporter<EcoesModel>.Statistics ImportStats;
            public int Lines;
            public int Buffers;
        }

        internal static Statistics Stats = new Statistics();

        const string ecoesFile = @"C:\Imp\Cron\XoserveEcoes\ecoes\ecoes.txt";

        static void Main(string[] args)
        {
            var timer = Stopwatch.StartNew();
            const int bufferSize = 100000;
            const int batchSize = 500000;
            using (var ecoes = new Importers.Ecoes(ecoesFile))
            {
                Console.WriteLine("Processing {0}", ecoesFile);
                Stats.ImportStats = ecoes.Stats;
                using (var batcher = new BufferedImporter<Importers.Ecoes, EcoesModel>(ecoes, bufferSize))
                {
                    using (var connection = new SqlConnection("Server=localhost;Initial Catalog=ENERGYWATCH.UAT;User ID=energywatch;Password=GraceSophie12")) // GraceSophie12
                    {
                        connection.Open();
                        using (var bulk = new SqlBulkCopy(connection, SqlBulkCopyOptions.KeepNulls | SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction, null))
                        {
                            bulk.DestinationTableName = "dbo.ecoes";
                            bulk.BatchSize = batchSize;
                            bulk.BulkCopyTimeout = 60;
                            bulk.EnableStreaming = true;
                            Console.WriteLine("Writing to SQLServer table {0}", bulk.DestinationTableName);
                            bulk.WriteToServer(batcher);
                        }
                    }
                    Stats.Buffers = batcher.Buffers;
                }
                Stats.Lines = ecoes.Lines;
            }
            timer.Stop();
            var total = timer.Elapsed;
            var perBatch = TimeSpan.FromTicks((long)Math.Ceiling((double)total.Ticks / Stats.Buffers));
            var perLine = TimeSpan.FromTicks((long)Math.Ceiling((double)total.Ticks / Stats.Lines));
            var perRecord = TimeSpan.FromTicks((long)Math.Ceiling((double)total.Ticks / Stats.ImportStats.Records));
            Console.WriteLine($"Processed {Stats.ImportStats.Records} record(s) in {Stats.Lines} lines. {Stats.Buffers} buffer(s). Skipped: {Stats.ImportStats.Skipped}");
            Console.WriteLine("Total\t\t\tBatch\t\t\tRecord\t\t\tLine");
            Console.WriteLine($"{total}\t{perBatch}\t{perRecord}\t{perLine}");
            Console.ReadKey();
        }
    }
}
