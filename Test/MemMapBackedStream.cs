﻿using System;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Threading.Tasks;

namespace Test
{
    using System.Threading;

    internal sealed class MemMapBackedStream : Stream, IHaveStream<MemMapBackedStream>
    {
        internal readonly string path;
        internal readonly FileStream fileStream;
        internal readonly MemoryMappedFileAccess fileAccess;
        internal readonly MemoryMappedFile mappedFile;
        private long currentOffset;
        private long bufferSize;
        private MemoryMappedViewStream viewStream;

        public MemMapBackedStream(string path, FileMode mode, FileAccess access, FileShare share, int currentOffset = 0, int bufferSize = 1024 * 1024 * 250)
        {
            this.path = path;
            fileStream = new FileStream(path, mode, access, share, 512, FileOptions.SequentialScan);
            fileAccess = access == FileAccess.Read ? MemoryMappedFileAccess.Read : access == FileAccess.Write ? MemoryMappedFileAccess.Write : MemoryMappedFileAccess.ReadWrite;
            mappedFile = MemoryMappedFile.CreateFromFile(fileStream, GetHashCode().ToString(), bufferSize, fileAccess, HandleInheritability.None, false);
            this.currentOffset = currentOffset;
            this.bufferSize = bufferSize;
            NextBuffer();
        }

        public MemMapBackedStream GetStream() => this;
        Stream IHaveStream.GetStream() => GetStream();

        public override bool CanRead => true;

        public override bool CanSeek => false;

        public override bool CanTimeout => false;

        public override bool CanWrite => false;

        public override long Length => bufferSize;

        public override long Position
        {
            get => viewStream.Position + currentOffset;
            set => throw new NotImplementedException();
        }

        public override void Close()
        {
            currentOffset = fileStream.Length;
            viewStream.Dispose();
            viewStream = null;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (currentOffset == fileStream.Length)
            {
                return -1;
            }

            int lastRead = 0;
            while ((lastRead += viewStream.Read(buffer, offset, count)) < count)
            {
                if (!NextBuffer())
                {
                    break;
                }
            }

            return lastRead;
        }

        public override Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return Task.FromCanceled<int>(cancellationToken);
            }
            if (currentOffset == fileStream.Length)
            {
                return Task.FromResult(-1);
            }
            return viewStream.ReadAsync(buffer, offset, count, cancellationToken);
        }

        public override int ReadByte()
        {
            var buf = new byte[1];
            if (Read(buf, 0, 1) == 1)
            {
                return buf[0];
            }
            return -1;
        }

        public override void Write(byte[] buffer, int offset, int count) => throw new NotImplementedException();
        public override void SetLength(long value) => throw new NotImplementedException();
        public override long Seek(long offset, SeekOrigin origin) => throw new NotImplementedException();

        public override void Flush()
        {
        }

        private bool NextBuffer()
        {
            var nextOffset = currentOffset + bufferSize;
            if (nextOffset >= fileStream.Length)
            {
                Close();
                return false;
            }
            viewStream = mappedFile.CreateViewStream(nextOffset, bufferSize, fileAccess);
            return true;
        }
    }
}
