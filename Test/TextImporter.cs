﻿using System;
using System.IO;
using System.Text;

namespace Test
{
    public abstract class TextImporter<TModel> : StreamImporter<TModel> where TModel : class, new()
    {
        private StreamReader textReader;

        private static TextReader MakeReader(Stream stream, Encoding encoding, bool detectEncoding, bool leaveOpen) => new StreamReader(stream, encoding, detectEncoding, 1024 * 512, leaveOpen);

        private static TextReader MakeReader(Stream stream, Encoding encoding, bool leaveOpen) => MakeReader(stream, encoding, false, leaveOpen);

        private static TextReader MakeReader(Stream stream, bool leaveOpen) => MakeReader(stream, Encoding.UTF8, true, leaveOpen);

        public static TextReader MakeReader(Stream stream) => MakeReader(stream, true);

        private static TextReader MakeReader(IHaveStream streamGetter) => MakeReader(streamGetter.GetStream());

        private TextImporter(string name, IHaveStream streamGetter, Stream stream, TextReader textReader) : base(name, streamGetter, stream)
        {
            //this.textReader = textReader;
        }

        protected TextImporter(string name, Stream stream) : base(name, stream)
        {
        }

        internal TextImporter(string name, IHaveStream streamGetter) : base(name, streamGetter)
        {
        }

        protected abstract TModel GetModelFromTextReader(TextReader textReader);

        protected sealed override TModel GetModelFromStream(Stream stream)
        {
            if (textReader == null)
            {
                textReader = new StreamReader(stream, Encoding.Default, true, 512, true);
            }
            return GetModelFromTextReader(textReader);
        }

        public override void Close()
        {
            textReader.Dispose();
            base.Close();
        }
    }

}
