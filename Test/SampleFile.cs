﻿using System;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Threading;
using System.Threading.Tasks;

namespace Test
{
    public sealed class SampleFile
    {
        private readonly string srcPath;
        private readonly string dstPath;

        private string NotNullOrWhiteSpace(string paramName, string path)
        {
            if (String.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullException(paramName);
            }
            return path;
        }

        public SampleFile(string srcPath, string dstPath)
        {
            this.srcPath = NotNullOrWhiteSpace("srcPath", srcPath);
            this.dstPath = NotNullOrWhiteSpace("dstPath", dstPath);
        }

        public void Prepare(int count)
        {
            Console.WriteLine("Creating input file stream");
            using (var inFileStream = new FileStream(srcPath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.SequentialScan))
            {
                Console.WriteLine("Creating input file map");
                using (var inMap = MemoryMappedFile.CreateFromFile(inFileStream, srcPath.Replace('\\', '_'), 0, MemoryMappedFileAccess.Read, null, HandleInheritability.None, true))
                {
                    Console.WriteLine("Creating input file map stream");
                    using (var inStream = inMap.CreateViewStream(0, 0, MemoryMappedFileAccess.Read))
                    {
                        Console.WriteLine("Creating input text reader");
                        StreamReader inReader;
                        using (inReader = new StreamReader(inStream, System.Text.Encoding.UTF8, false, 1024 * 1024 * 10, true))
                        {
                            Console.WriteLine("Creating output file stream");
                            using (var outFileStream = new FileStream(dstPath, FileMode.OpenOrCreate | FileMode.Truncate, FileAccess.ReadWrite, FileShare.ReadWrite, 4096, FileOptions.SequentialScan | FileOptions.WriteThrough))
                            {
                                Console.WriteLine("Creating output text reader");
                                using (var outWriter = new StreamWriter(outFileStream, System.Text.Encoding.UTF8, 1024 * 1024 * 10, true))
                                {
                                    Console.WriteLine("Copying lines");
                                    outWriter.WriteLine(inReader.ReadLine());
                                    for (int lineCount = 0; lineCount < count; lineCount++)
                                    {
                                        outWriter.WriteLine(inReader.ReadLine());
                                    }
                                    
                                    inStream.Seek(inFileStream.Length - 1024, SeekOrigin.Begin);
                                    var buf = new byte[1024];
                                    var reader = new StringReader(System.Text.Encoding.UTF8.GetString(buf, 0, inStream.Read(buf, 0, 1024)));
                                    string lastLine = String.Empty;

                                    while (true)
                                    {
                                        var line = reader.ReadLine();
                                        if (String.IsNullOrWhiteSpace(line))
                                        {
                                            break;
                                        }
                                        lastLine = line;
                                    }
                                    outWriter.WriteLine(lastLine);
                                }
                            }
                        }
                    }
                }
            }
        }

        public async Task PrepareAsync(int count, CancellationToken cancellationToken)
        {
            using (var inFileStream = new FileStream(srcPath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.SequentialScan | FileOptions.Asynchronous))
            {
                using (var inMap = MemoryMappedFile.CreateFromFile(inFileStream, srcPath, 0, MemoryMappedFileAccess.Read, null, HandleInheritability.None, true))
                {
                    using (var inStream = inMap.CreateViewStream(0, 0, MemoryMappedFileAccess.Read))
                    {
                        using (var inReader = new StreamReader(inStream, System.Text.Encoding.UTF8, false, 1024 * 1024 * 10, true))
                        {
                            using (var outFileStream = new FileStream(dstPath, FileMode.OpenOrCreate | FileMode.Truncate, FileAccess.ReadWrite, FileShare.ReadWrite, 4096, FileOptions.SequentialScan | FileOptions.WriteThrough | FileOptions.Asynchronous))
                            {
                                using (var outMap = MemoryMappedFile.CreateFromFile(outFileStream, dstPath, 0, MemoryMappedFileAccess.ReadWrite, null, HandleInheritability.None, true))
                                {
                                    using (var outStream = outMap.CreateViewStream(0, 0, MemoryMappedFileAccess.ReadWrite))
                                    {
                                        using (var outWriter = new StreamWriter(outStream, System.Text.Encoding.UTF8, 1024 * 1024 * 10, true))
                                        {
                                            for (int lineCount = 0; lineCount < count; lineCount++)
                                            {
                                                await outWriter.WriteLineAsync(await inReader.ReadLineAsync());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public Task PrepareAsync(int count)
        {
            return PrepareAsync(count, CancellationToken.None);
        }
    }
}
