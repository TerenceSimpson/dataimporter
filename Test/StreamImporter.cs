﻿using System;
using System.IO;

namespace Test
{
    public abstract class StreamImporter<TModel> : StronglyTypedDbDataReader<TModel> where TModel : class, new()
    {
        private readonly string path;
        private readonly IHaveStream streamGetter;
        private readonly Stream stream;

        public Stream BaseStream => stream;

        public override void Close()
        {
            base.Close();
            stream.Dispose();
            streamGetter?.Dispose();
        }

        internal StreamImporter(string path, IHaveStream streamGetter, Stream stream, int depth) : base(depth)
        {
            this.path = path;
            this.streamGetter = streamGetter;
            this.stream = streamGetter == null ? stream : streamGetter.GetStream();
        }

        internal StreamImporter(string path, IHaveStream streamGetter, Stream stream) : this(path, streamGetter, stream, 1)
        {
        }

        protected StreamImporter(string path, Stream stream) : this(path, null, stream ?? throw new ArgumentNullException(nameof(stream)))
        {
        }

        internal StreamImporter(string path, IHaveStream streamGetter) : this(path, streamGetter ?? throw new ArgumentNullException(nameof(streamGetter)), null)
        {
        }

        internal StreamImporter(IHaveStream streamGetter) : this(String.Empty, streamGetter)
        {
            this.streamGetter = streamGetter;
        }

        protected StreamImporter(string path, FileMode mode, FileAccess access, FileShare share) : this(path, new FileStream(path, mode, access, share, 4096, FileOptions.SequentialScan))
        {
        }

        protected override TModel GetModel()
        {
            return GetModelFromStream(BaseStream);
        }

        protected abstract TModel GetModelFromStream(Stream stream);
    }

}
