﻿namespace Test
{
    public class BufferedImporter<TImporter, TModel> : StronglyTypedDbDataReader<TModel> where TImporter : StronglyTypedDbDataReader<TModel> where TModel : class, new()
    {
        internal sealed class Buffer
        {
            private readonly int bufferCapacity;
            private int bufferSize;
            private int nextIndex;
            private TModel[] buffer;
            
            public Buffer(int bufferCapacity)
            {
                this.bufferCapacity = bufferCapacity;
            }

            public bool Fill(StronglyTypedDbDataReader<TModel> importer)
            {
                if (buffer != null)
                {
                    buffer = null;
                }

                buffer = new TModel[bufferCapacity];
                int bufIdx = 0;
                for (; bufIdx < bufferCapacity && importer.Read(); bufIdx++)
                {
                    buffer[bufIdx] = importer.CurrentModel;
                }
                bufferSize = bufIdx;
                nextIndex = 0;

                if (bufIdx == 0)
                {
                    buffer = null;
                    return false;
                }
                return true;
            }

            public bool CanRead => nextIndex < bufferSize;

            public TModel Read()
            {
                if (!CanRead)
                {
                    return null;
                }
                var thisIndex = nextIndex;
                nextIndex++;
                return buffer[thisIndex];
            }
        }

        private readonly TImporter importer;
        private readonly Buffer buffer;
        private int buffers;

        public int Buffers => buffers;

        public BufferedImporter(TImporter importer, int bufferSize, int depth) : base(depth)
        {
            this.importer = importer;
            buffer = new Buffer(bufferSize);
        }

        public BufferedImporter(TImporter importer, int bufferSize) : this(importer, bufferSize, 1)
        {
        }

        protected virtual bool FillBuffer()
        {
            if (!buffer.Fill(importer))
            {
                return false;
            }
            ++buffers;
            return true;
        }

        protected virtual TModel GetModelFromBuffer()
        {
            if (!buffer.CanRead)
            {
                if (!FillBuffer())
                {
                    return null;
                }
            }
            return buffer.Read();
        }

        protected sealed override TModel GetModel()
        {
            return GetModelFromBuffer();
        }

        public override void Close()
        {
            importer.Close();
            base.Close();
        }
    }

}
