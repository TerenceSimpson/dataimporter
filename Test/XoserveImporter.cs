﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading.Tasks;

namespace Test
{
    using XoserveReader = StronglyTypedDbDataReader<XoserveModel>;

    class XoserveImporter : XoserveReader
    {
        private readonly string path;
        private readonly IHaveStream streamBase;
        private readonly StreamReader streamReader;

        const int bufferLength = 100000;
        private XoserveModel[] buffer;
        private int nextIndex = 0;


        public XoserveImporter(string path)
        {
            this.path = path;
            streamBase = HaveStream.GetStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
            var backingStream = streamBase.GetStream();
            Console.WriteLine($"Backing stream: {backingStream.GetType()}");
            streamReader = new StreamReader(backingStream, Encoding.UTF8, false, 4096, false);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                streamReader.Dispose();
                streamBase.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FillBuffer()
        {
            buffer = new XoserveModel[bufferLength];
            var fillIdx = 0;
            for (fillIdx = 0; fillIdx < buffer.Length; fillIdx++)
            {
                var model = ReallyGetModel();
                if (model == null)
                {
                    break;
                }
                if (ReferenceEquals(model, SkipObject))
                {
                    GetStats().Skipped++;
                    --fillIdx;
                    continue;
                }
                buffer[fillIdx] = model;
            }
            if (fillIdx == 0)
            {
                buffer = null;
                nextIndex = 0;
                return false;
            }
            //GetStats().Buffers++;
            nextIndex = 0;
            return true;
        }

        private static readonly Regex splitter = new Regex("(?:^|,)(\"(?:[^\"]+|\"\")*\"|[^,]*)", RegexOptions.Compiled);

        private static string[] SplitLine(string line)
        {
            return (from Match match in splitter.Matches(line)
                    select match.Value.TrimStart(',').TrimStart('"').TrimEnd('"').Replace(",", "")).ToArray();
        }

        private bool ShouldSkip(object[] parts) => parts == null || parts.Length < 5;

        private XoserveModel ReallyGetModel()
        {
            string line;
            if (streamReader.EndOfStream || (line = streamReader.ReadLine()) == null)
            {
                return null;
            }
            //GetStats().Lines++;
            if (String.IsNullOrWhiteSpace(line))
            {
                return SkipObject;
            }
            var parts = SplitLine(line);

            if (ShouldSkip(parts))
            {
                return SkipObject;
            }

            return NewModel(parts);
        }

        protected override XoserveModel GetModel()
        {
            if (buffer == null || nextIndex >= buffer.Length)
            {
                if (!FillBuffer())
                {
                    return null;
                };
            }
            var model = buffer[nextIndex];
            if (model == null)
            {
                nextIndex = buffer.Length;
                return GetModel();
            }
            nextIndex++;
            return model;
        }
    }
}
