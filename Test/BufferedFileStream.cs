﻿using System.IO;

namespace Test
{
    internal sealed class BufferedFileStream : IHaveStream<BufferedStream>
    {
        private readonly string path;
        private readonly FileStream fileStream;
        private readonly BufferedStream bufferedStream;
        private readonly int bufferSize;

        public BufferedFileStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize)
        {
            this.path = path;
            this.bufferSize = bufferSize;
            fileStream = new FileStream(path, mode, access, share, 4096 * 4, FileOptions.RandomAccess);
            bufferedStream = new BufferedStream(fileStream, bufferSize);
        }

        public BufferedStream GetStream() => bufferedStream;
        Stream IHaveStream.GetStream() => GetStream();

        public void Dispose() => Dispose(true);

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                bufferedStream.Dispose();
                fileStream.Dispose();
            }
        }
    }
}
