﻿using System.IO;
using System.IO.MemoryMappedFiles;

namespace Test
{
    internal sealed class MemMapStream : IHaveStream<MemoryMappedViewStream>, IHaveStream
    {
        internal readonly string path;
        internal readonly FileStream fileStream;
        internal readonly MemoryMappedFileAccess fileAccess;
        internal readonly MemoryMappedFile mappedFile;
        internal readonly MemoryMappedViewStream viewStream;

        public MemMapStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize)
        {
            this.path = path;
            fileStream = new FileStream(path, mode, access, share, 512, FileOptions.RandomAccess);
            switch (access)
            {
                case FileAccess.Read:
                    fileAccess = MemoryMappedFileAccess.Read;
                    break;
                case FileAccess.Write:
                    fileAccess = MemoryMappedFileAccess.Write;
                    break;
                case FileAccess.ReadWrite:
                    fileAccess = MemoryMappedFileAccess.ReadWrite;
                    break;
                default:
                    break;
            }
            mappedFile = MemoryMappedFile.CreateFromFile(fileStream, GetHashCode().ToString(), 0, fileAccess, HandleInheritability.None, false);
            viewStream = mappedFile.CreateViewStream(0, 0, fileAccess);
        }

        public MemoryMappedViewStream GetStream() => viewStream;
        Stream IHaveStream.GetStream() => GetStream();

        public void Dispose() => Dispose(true);

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                viewStream.Dispose();
                mappedFile.Dispose();
                fileStream.Dispose();
            }
        }
    }
}
