﻿using System.IO;

namespace Test
{
    internal static class HaveStream
    {
        public static IHaveStream GetStream(string path, FileMode mode = FileMode.Open, FileAccess access = FileAccess.Read, FileShare share = FileShare.Read, int bufferSize = 1024 * 512)
        {
            try
            {
                return new MemMapStream(path, mode, access, share, bufferSize);
            }
            catch
            {
                try
                {
                    return new MemMapBackedStream(path, mode, access, share, 0, bufferSize);
                }
                catch
                {
                    return new BufferedFileStream(path, mode, access, share, bufferSize);
                }
            }
        }
    }
}
