﻿namespace Test
{
    public sealed class NewEcoesModel
    {
        public string MPAN;
        public string GSP;
        public string Line1;
        public string Line2;
        public string Line3;
        public string Line4;
        public string Line5;
        public string Line6;
        public string Line7;
        public string Line8;
        public string Line9;
        public string PostCode;
        public string UPRN;
        public string MSN;
        public string MeterType;
        public string ProfileClass;
        public string MTC;
        public string LLF;
        public string SSC;
        public string EnergisationStatus;
        public string EnergisationStatusFrom;
        public string InstallingSupplier;
        public string ServiceFlag;
        public string SMSO;
        public string IHD;
        public string SMETS;
        public string MeasurementClass;
        public string GreenDeal;
    }

    public sealed class EcoesModel
    {
        public string mpan;
        public string gsp;
        public string address_1;
        public string address_2;
        public string address_3;
        public string address_4;
        public string address_5;
        public string address_6;
        public string address_7;
        public string address_8;
        public string address_9;
        public string post_code;
        public string number;
        public string meter;
        public string meter_type;
        public string pc;
        public string mtc;
        public string llf;
        public string ssc;
        public string es;
        public string es_date;
        public string temp1;
        public string temp2;
        public string temp3;
        public string temp4;
        public string temp5;
    }

    public static class EcoesRecordExtension
    {
        public static bool IsNull(this EcoesModel self) => self == null || self.post_code == null;
        public static bool IsNull(this NewEcoesModel self) => self == null || self.PostCode == null;
    }
}
