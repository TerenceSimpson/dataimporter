﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    public abstract class StronglyTypedDbDataReader<TModel> : DbDataReader, IEnumerable<TModel>, IEnumerable where TModel : class, new()
    {
        internal interface IMemberAccessor
        {
            string GetName();
            Type GetDataType();
            object GetValue(TModel model);
            void SetValue(TModel model, object value);
        }

        internal interface IMemberAccessor<TField> : IMemberAccessor
        {
            new TField GetValue(TModel model);
            void SetValue(TModel model, TField value);
        }

        internal sealed class MemberAccessor<TField> : IMemberAccessor<TField>, IMemberAccessor
        {
            private readonly string name;
            private readonly Func<TModel, TField> getter;
            private readonly Action<TModel, TField> setter;

            private MemberAccessor(string name, Func<TModel, TField> getter, Action<TModel, TField> setter)
            {
                this.name = name;
                this.getter = getter;
                this.setter = setter;
            }

            private static void CheckFieldType(Type type)
            {
                if (type != typeof(TField))
                {
                    throw new InvalidOperationException("Wrong type :(");
                }
            }

            public MemberAccessor(FieldInfo info) : this(info.Name, (model) => (TField)info.GetValue(model), (model, value) => info.SetValue(model, value)) => CheckFieldType(info.FieldType);

            public MemberAccessor(PropertyInfo info) : this(info.Name, (model) => (TField)info.GetValue(model), (model, value) => info.SetValue(model, value)) => CheckFieldType(info.PropertyType);

            public static MemberAccessor<TField> Either(FieldInfo fieldInfo, PropertyInfo propertyInfo) => fieldInfo != null ? new MemberAccessor<TField>(fieldInfo) : new MemberAccessor<TField>(propertyInfo);

            public string GetName() => name;

            public Type GetDataType() => typeof(TField);

            public TField GetValue(TModel model) => getter(model);

            public void SetValue(TModel model, TField value) => setter(model, value);

            object IMemberAccessor.GetValue(TModel model)
            {
                return (object)GetValue(model) ?? DBNull.Value;
            }

            void IMemberAccessor.SetValue(TModel model, object value)
            {
                if (Convert.IsDBNull(value))
                {
                    value = null;
                }
                SetValue(model, (TField)value);
            }
        }

        internal static class MemberAccessorHelper
        {
            public static IMemberAccessor Either(FieldInfo fieldInfo, PropertyInfo propertyInfo)
            {
                var dataType = fieldInfo?.FieldType ?? propertyInfo.PropertyType;
                var accessorType = typeof(MemberAccessor<>);
                var newAccessorType = accessorType.MakeGenericType(typeof(TModel), dataType);
                return (IMemberAccessor)newAccessorType.InvokeMember(String.Empty, BindingFlags.Public | BindingFlags.Instance | BindingFlags.CreateInstance, Type.DefaultBinder, null, new object[1] { fieldInfo ?? (object)propertyInfo });
                //return fieldInfo != null ? new MemberAccessor<TField>(fieldInfo) : new MemberAccessor<TField>(propertyInfo);
            }
        }

        internal sealed class EnumeratorWrapper : IEnumerator<TModel>, IEnumerator
        {
            private readonly StronglyTypedDbDataReader<TModel> reader;

            public EnumeratorWrapper(StronglyTypedDbDataReader<TModel> reader) => this.reader = reader;

            TModel IEnumerator<TModel>.Current => reader.model;

            object IEnumerator.Current => reader.model;

            bool IEnumerator.MoveNext() => reader.Read();

            void IEnumerator.Reset() => throw new NotSupportedException();

            void IDisposable.Dispose()
            {
            }
        }

        public class Statistics : ICloneable
        {
            public int Records;
            public int Skipped;

            public Statistics Clone() => new Statistics
            {
                Records = Records,
                Skipped = Skipped
            };

            object ICloneable.Clone() => Clone();
        }

        internal static IMemberAccessor[] GetMemberAccessors()
        {
            return (from member in typeof(TModel).GetMembers()
                    let isField = member.MemberType == MemberTypes.Field
                    let isProperty = member.MemberType == MemberTypes.Property
                    where isField || isProperty
                    select MemberAccessorHelper.Either(member as FieldInfo, member as PropertyInfo)).ToArray();
        }

        private static IMemberAccessor[] memberAccessors = GetMemberAccessors();

        private readonly int depth = 1;
        private Statistics stats = new Statistics();
        private TModel model;

        public Statistics GetStats() => stats;
        public Statistics Stats => stats;

        public static readonly TModel SkipObject = new TModel();

        public override int Depth => depth;

        public override int FieldCount => memberAccessors.Length;

        public override bool HasRows => true;

        public override bool IsClosed => model == null;

        public override int RecordsAffected => IsClosed ? stats.Records : -1;

        public override object this[string name] => GetValue(GetOrdinal(name));

        public override object this[int ordinal] => GetValue(ordinal);

        public StronglyTypedDbDataReader(int depth) => this.depth = depth;

        public StronglyTypedDbDataReader() : this(1)
        {
        }

        public override bool NextResult()
        {
            return false;
        }

        protected abstract TModel GetModel();

        private bool TryRead(out TModel model)
        {
            return (model = GetModel()) != null;
            
        }

        public sealed override bool Read()
        {
            if (!TryRead(out model))
            {
                return false;
            }

            while (ReferenceEquals(model, SkipObject))
            {
                stats.Skipped++;
                if (!TryRead(out model))
                {
                    return false;
                }
            }
            stats.Records++;
            return true;
        }

        public override void Close()
        {
            base.Close();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public override string GetDataTypeName(int ordinal) => GetFieldType(ordinal).Name;

        public override Type GetFieldType(int ordinal) => memberAccessors[CheckRange(ordinal)].GetDataType();

        public override string GetName(int ordinal) => memberAccessors[CheckRange(ordinal)].GetName();

        public override int GetOrdinal(string name) => Array.FindIndex(memberAccessors, (accessor) => accessor.GetName().Equals(name, StringComparison.CurrentCultureIgnoreCase));

        public override DataTable GetSchemaTable()
        {
            return base.GetSchemaTable();
        }

        public override bool GetBoolean(int ordinal) => GetFieldValue<bool>(ordinal);

        public override byte GetByte(int ordinal) => GetFieldValue<byte>(ordinal);

        public override long GetBytes(int ordinal, long dataOffset, byte[] buffer, int bufferOffset, int length)
        {
            if (TryGetFieldValue(ordinal, out byte[] byteValue))
            {
                Array.ConstrainedCopy(byteValue, (int)dataOffset, buffer, bufferOffset, length);
                return byteValue.Length - dataOffset < length ? byteValue.Length - dataOffset : length;
            }
            else if (TryGetFieldValue(ordinal, out char[] charValue))
            {
                var byteArray = Encoding.Default.GetBytes(charValue);
                Array.ConstrainedCopy(byteArray, (int)dataOffset, buffer, bufferOffset, length);
                return byteArray.Length - dataOffset < length ? byteArray.Length - dataOffset : length;
            }
            else if (TryGetFieldValue(ordinal, out string stringValue))
            {
                var charArray = stringValue.ToCharArray();
                var byteArray = Encoding.Default.GetBytes(charArray);
                Array.ConstrainedCopy(byteArray, (int)dataOffset, buffer, bufferOffset, length);
                return byteArray.Length - dataOffset < length ? byteArray.Length - dataOffset : length;
            }
            throw new NotImplementedException();
        }

        public override char GetChar(int ordinal) => GetFieldValue<char>(ordinal);

        public override long GetChars(int ordinal, long dataOffset, char[] buffer, int bufferOffset, int length)
        {
            if (TryGetFieldValue(ordinal, out char[] charValue))
            {
                Array.ConstrainedCopy(charValue, (int)dataOffset, buffer, bufferOffset, length);
                return charValue.Length - dataOffset < length ? charValue.Length - dataOffset : length;
            }
            else if(TryGetFieldValue(ordinal, out string stringValue))
            {
                var charArray = stringValue.ToCharArray();
                Array.ConstrainedCopy(charArray, (int)dataOffset, buffer, bufferOffset, length);
                return charArray.Length - dataOffset < length ? charArray.Length - dataOffset : length;
            }
            throw new NotImplementedException();
        }

        protected override DbDataReader GetDbDataReader(int ordinal)
        {
            
            return base.GetDbDataReader(ordinal);
        }

        public override DateTime GetDateTime(int ordinal) => GetFieldValue<DateTime>(ordinal);

        public override decimal GetDecimal(int ordinal) => GetFieldValue<decimal>(ordinal);

        public override double GetDouble(int ordinal) => GetFieldValue<double>(ordinal);

        public override float GetFloat(int ordinal) => GetFieldValue<float>(ordinal);

        public override Guid GetGuid(int ordinal) => GetFieldValue<Guid>(ordinal);

        public override short GetInt16(int ordinal) => GetFieldValue<short>(ordinal);

        public override int GetInt32(int ordinal) => GetFieldValue<int>(ordinal);

        public override long GetInt64(int ordinal) => GetFieldValue<long>(ordinal);

        public override string GetString(int ordinal) => GetValue(ordinal).ToString();

        public override Stream GetStream(int ordinal)
        {
            return base.GetStream(ordinal);
        }

        public override TextReader GetTextReader(int ordinal)
        {
            return base.GetTextReader(ordinal);
        }

        public override object GetValue(int ordinal) => memberAccessors[CheckRange(ordinal)].GetValue(model);

        public override T GetFieldValue<T>(int ordinal)
        {
            CheckRange(ordinal);
            var requestType = typeof(T);
            var nullableType = Nullable.GetUnderlyingType(requestType);
            var valueType = nullableType ?? requestType;
            var accessor = memberAccessors[ordinal];
            var memberType = accessor.GetDataType();
            var value = accessor.GetValue(model);

            if (valueType.IsAssignableFrom(memberType))
            {
                if (nullableType != null && Convert.IsDBNull(value))
                {
                    if (nullableType != null)
                    {
                        return default(T);
                    }
                }
                return (T)Convert.ChangeType(value, requestType);
            }
            else
            {
                if (nullableType != null && Convert.IsDBNull(value))
                {
                    return default(T);
                }
                return (T)value;
            }
        }

        public bool TryGetFieldValue<T>(int ordinal, out T value)
        {
            value = default(T);
            try
            {
                value = GetFieldValue<T>(ordinal);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private int CheckRange(int ordinal)
        {
            if (ordinal < 0 || ordinal > memberAccessors.Length)
            {
                throw new IndexOutOfRangeException();
            }
            return ordinal;
        }

        public override int GetValues(object[] values)
        {
            if (values == null || values.Length == 0)
            {
                throw new ArgumentNullException(nameof(values));
            }
            var len = values.Length < memberAccessors.Length ? values.Length : memberAccessors.Length;
            for (int idx = 0; idx < len; idx++)
            {
                values[idx] = memberAccessors[idx].GetValue(model);
            }
            return len;
        }

        public TModel CurrentModel => model ?? throw new InvalidOperationException("No current model");

        public override bool IsDBNull(int ordinal) => Convert.IsDBNull(GetValue(ordinal));

        public override IEnumerator GetEnumerator() => ((IEnumerable<TModel>)this).GetEnumerator();

        IEnumerator<TModel> IEnumerable<TModel>.GetEnumerator() => new EnumeratorWrapper(this);

        public static TModel NewModel(params object[] fields)
        {
            var model = new TModel();
            var len = fields.Length < memberAccessors.Length ? fields.Length : memberAccessors.Length;
            for (int idx = 0; idx < len; idx++)
            {
                memberAccessors[idx].SetValue(model, fields[idx]);
            }
            return model;
        }

        public static bool TryNewModel(out TModel model, params object[] fields)
        {
            model = null;
            try
            {
                model = NewModel(fields);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
